# README #
For those who hate the Arduino IDE, but seriously, it's an easy way to get something going at home, this is a Windows based build/upload script so people can use their own editor and serial programs, or even set up a CD/CI server based on it. Now that's not a bad idea is it?

### How do I get set up? ###

Put the build script in the directory of the .ino file, and check the following variables

This is the directory of your Arduino IDE install:
`set ARDUINO_BUILD_COMMAND="C:\Program Files (x86)\Arduino\arduino_debug.exe"`

Here we setup what kind of Arduino you are using. This is using an ESP8266 Sparkfun Thing, but can be changed to any installed board.
I reccommed using the IDE to do a full verify with verbosity and all warnings on, and it will tell you the following settings at the top of the messages.

The format is: `package:arch:board[:parameters]]`
 
`set BOARD_PARAMS="esp8266:esp8266:thing:CpuFrequency=80,UploadSpeed=115200"`

Serial port the Arduino is connected to

`set UPLOAD_PORT="COM3"`

Source project to compile. You only need to define the base .ino

`set ARDUINO_PROJECT="Jormungar.ino"`

### Use ###
Run it, and a prompt will come up, asking what you want to do, enter an option and off you go!

### Reccomendations
Hey guys, I really love using Sublime Text 3 ([here](https://www.sublimetext.com/3)) and Extra Putty ([here](http://www.extraputty.com/)). Do reccommend. Extra putty lets you easily hang up the serial connection before an upload, without closing the entire interface, and sublime text? Well, best text editor I've ever used.

