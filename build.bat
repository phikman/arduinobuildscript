@echo off
rem Build/Upload script for Arduino

set VERSION=1.0

set ARDUINO_BUILD_COMMAND="C:\Program Files (x86)\Arduino\arduino_debug.exe"
set BOARD_PARAMS="esp8266:esp8266:thing:CpuFrequency=80,UploadSpeed=115200"
set UPLOAD_PORT="COM3"
set ARDUINO_PROJECT="Jormungar.ino"

echo ==============================================
echo Phill Ogden\'s Arduino build/upload script
echo January 2017
echo Version %VERSION%
echo ==============================================

:loop
set /p COMMAND="Enter option. `b` builds `u` uploads `a` archives `q` quits: "

IF %COMMAND%==b (
	echo Building %ARDUINO_PROJECT%
	%ARDUINO_BUILD_COMMAND% --verify --board %BOARD_PARAMS% %ARDUINO_PROJECT%

	IF %ERRORLEVEL% NEQ 0 (
		echo Build Failed!!
	) ELSE (
		echo Build Completed!
	)
)

IF %COMMAND%==a (
	echo Building %ARDUINO_PROJECT% for archive
	rmdir /S /Q ".\__build"
	mkdir ".\__build"
	%ARDUINO_BUILD_COMMAND% --pref build.path=".\__build" --verify --board %BOARD_PARAMS% %ARDUINO_PROJECT%
	IF %ERRORLEVEL% NEQ 0 (
		echo Build and Archive Failed!!
	) ELSE (
		echo Archiving...
		for /F "tokens=1-4 delims=/ " %%A in ('date/t') do (
		set DateDay=%%A
		set DateMonth=%%B
		set DateYear=%%C
		)

		set CurrentDate=%DateDay%-%DateMonth%-%DateYear%-%time:~0,2%.%time:~3,2%

		mkdir ".\Build_%CurrentDate%"

		xcopy /s ".\__build" ".\Build_%CurrentDate%"

		echo Build and Archive Completed!
	)

	rmdir /S /Q ".\__build"
)

IF %COMMAND%==u (
	echo Uploading %ARDUINO_PROJECT% to %UPLOAD_PORT%
	%ARDUINO_BUILD_COMMAND% --board %BOARD_PARAMS% --port %UPLOAD_PORT% --upload %ARDUINO_PROJECT%
	IF %ERRORLEVEL% NEQ 0 (
		echo Build and Upload Failed!!
	) ELSE (
		echo Build and Upload Completed!
	)
) 

IF %COMMAND%==q (
	goto quit
)

goto loop

:quit
echo Exiting...